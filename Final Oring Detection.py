# Oring detection - By: KL Solutions - Written by Kevin LeFrank- Mon Jul 26 2021

# Template Matching - Normalized Cross Correlation (NCC)
#
# Open MV documentation at time of writing :
# This example shows off how to use the NCC feature of your OpenMV Cam to match
# image patches to parts of an image... expect for extremely controlled enviorments
# NCC is not all to useful.
#
# WARNING: NCC supports needs to be reworked! As of right now this feature needs
# a lot of work to be made into somethin useful. This script will reamin to show
# that the functionality exists, but, in its current state is inadequate.

import time, sensor, image, pyb
from image import SEARCH_EX, SEARCH_DS
from pyb import Pin, LED


#GoodPin = Pin('P0', Pin.OUT_PP, Pin.PULL_DOWN)
GoodPin = Pin('P0', Pin.OUT_PP, Pin.PULL_UP)
BadPin = Pin('P1', Pin.OUT_PP, Pin.PULL_UP)

# Reset sensor
sensor.reset()

# Set sensor settings
sensor.set_contrast(0)  # CONTROLS THE AMOUNT OF WHITE IN THE IMAGE
sensor.set_gainceiling(4)   #CONTROLS THE DEPTH OF BLACK IN THE IMAGE


# Max resolution for template matching with SEARCH_EX is QQVGA
sensor.set_framesize(sensor.QQVGA)
#sensor.set_framesize(sensor.QVGA)
# You can set windowing to reduce the search image.
#sensor.set_windowing(((640-80)//2, (480-60)//2, 80, 60))
sensor.set_pixformat(sensor.GRAYSCALE)

# Load template.
# Template should be a small (eg. 32x32 pixels) grayscale image.
#TEMPLATES MUST BE .pgm FILES.  YOU CAN MAKE THESE IN gimp.  FILES MUST BE LOCAL TO SD CARD
noPin= image.Image("/noPin.pgm")
noOringBrass= image.Image("/BrassNoOring.pgm")
noOringSS= image.Image("/SSNoOring.pgm")

#dnoOringBrass= image.Image("/dBrassNoOring.pgm")
#dnoOringSS= image.Image("/dSSnoOring.pgm")
#darkBrassNoOring = image.Image("/darkBrassNoOring.pgm")
#darkDarkBrassNoOring = image.Image("/darkDarkBrassNoOring.pgm")
#darkSSnoOring = image.Image("/darkSSnoOring.pgm")
#darkdarkSSnoOring = image.Image("/darkdarkSSnoOring.pgm")
#lightSSnoOring = image.Image("/lightSSnoOring.pgm")
#lightBrassNoOring = image.Image("/lightBrassNoOring.pgm")


BrassBlackOring= image.Image("/BrassBlackOring.pgm")
BrassBrownOring= image.Image("/BrassBrownOring.pgm")
#dBrassBlackOring= image.Image("/dBrassBlackOring.pgm")
#dBrassBrownOring= image.Image("/dBrassBrownOring.pgm")

ssBlackOring= image.Image("/SSBlackOring.pgm")
ssBrownOring= image.Image("/SSBrownOring.pgm")
ssRedOring= image.Image("/SSRedOring.pgm")
ssPurpleOring= image.Image("/SSPurpleOring.pgm")
#dssBlackOring= image.Image("/dssBlackOring.pgm")
#dssBrownOring= image.Image("/dssBrownOring.pgm")
#dssRedOring= image.Image("/dssRedOring.pgm")
#dssPurpleOring= image.Image("/dssPurpleOring.pgm")
#darkdarkssBrownOring = image.Image("/darkdarkssBrownOring.pgm")
#darkdarkssBrownOring2 = image.Image("/darkdarkssBrownOring2.pgm")

#LED(4).on()
LED(3).on()
LED(2).on()
LED(1).on()
clock = time.clock()

def GoodPart() :
    GoodPin.value(1)
    LED(1).off()
    LED(3).off()
    pyb.delay(500)
    LED(1).on()
    LED(3).on()
    pyb.delay(2000)
    GoodPin.value(0)

def BadPart():
    BadPin.value(1)
    LED(2).off()
    LED(3).off()
    pyb.delay(500)
    LED(2).on()
    LED(3).on()
    pyb.delay(2000)
    BadPin.value(0)


# Run template matching


while (True):

    clock.tick()
    img = sensor.snapshot()

    img.draw_rectangle(45,20,65,80)


    np = img.find_template(noPin, 0.60, step=4, search=SEARCH_EX , roi=(45,20,65,80) )
    if np:
        img.draw_rectangle(np)
        print("NO PIN NO PIN NO PIN")
        #BadPart()

    bno = img.find_template(noOringBrass, 0.70, step=2, search=SEARCH_EX , roi=(45,20,65,80))
 #   dbno = img.find_template(dnoOringBrass, 0.70, step=2, search=SEARCH_EX , roi=(45,20,65,80))
 #   dbno2 = img.find_template(darkBrassNoOring  , 0.70, step=2, search=SEARCH_EX, roi=(45,20,65,80))
 #   dbno3 = img.find_template(darkDarkBrassNoOring, 0.70, step=2, search=SEARCH_EX, roi=(45,20,65,80))
 #  lbno = img.find_template(lightBrassNoOring, 0.70, step=2, search=SEARCH_EX, roi=(45,20,65,80))
    if bno:
        img.draw_rectangle(bno)
        print("NO ORING NO ORING NO ORING")
        BadPart()
 #!    elif dbno:
 #       img.draw_rectangle(dbno)
 #       print("NO ORING NO ORING NO ORING")
 #        BadPart()
 #   elif dbno2:
 #       img.draw_rectangle(dbno2)
 #       print("NO ORING NO ORING NO ORING")
 #       BadPart()
 #   elif dbno3:
 #       img.draw_rectangle(dbno3)
 #       print("NO ORING NO ORING NO ORING")
 #       BadPart()
 #   elif lbno:
 #       img.draw_rectangle(lbno)
 #       print("NO ORING NO ORING NO ORING")
 #       BadPart()

    ssno = img.find_template(noOringSS, 0.70, step=2, search=SEARCH_EX , roi=(45,20,65,80))
  #  dssno = img.find_template(dnoOringSS, 0.70, step=2, search=SEARCH_EX , roi=(45,20,65,80))
  #  ddssno = img.find_template(darkSSnoOring, 0.70, step=2, search=SEARCH_EX , roi=(45,20,65,80))
  #  ddssno2 = img.find_template(darkdarkSSnoOring, 0.70, step=2, search=SEARCH_EX , roi=(45,20,65,80))
  #  lssno = img.find_template(lightSSnoOring, 0.70, step=2, search=SEARCH_EX , roi=(45,20,65,80))
    if ssno :
        img.draw_rectangle(ssno)
        print("NO ORING NO ORING NO ORING")
        BadPart()
  #  elif dssno :
  #      img.draw_rectangle(dssno)
  #      print("NO ORING NO ORING NO ORING")
  #      BadPart()
  #  elif ddssno :
  #      img.draw_rectangle(ddssno)
  #      print("NO ORING NO ORING NO ORING")
  #      BadPart()
  #  elif ddssno2 :
  #      img.draw_rectangle(ddssno2)
  #      print("NO ORING NO ORING NO ORING")
  #      BadPart()
  #  elif lssno :
  #      img.draw_rectangle(lssno)
  #      print("NO ORING NO ORING NO ORING")
  #      BadPart()


    bBrO = img.find_template(BrassBrownOring, 0.60, step=2, search=SEARCH_EX , roi=(45,20,65,80))
   # dbBrO = img.find_template(dBrassBrownOring, 0.60, step=2, search=SEARCH_EX , roi=(45,20,65,80))
    if bBrO:
        img.draw_rectangle(bBrO)
        print("Brass Brown")
        GoodPart()
    #elif dbBrO:
    #    img.draw_rectangle(dbBrO)
    #    print("Brass Brown")
    #    GoodPart()

    bBkO= img.find_template(BrassBlackOring, 0.60, step=2, search=SEARCH_EX , roi=(45,20,65,80))
    #dbBkO= img.find_template(dBrassBlackOring, 0.60, step=2, search=SEARCH_EX , roi=(45,20,65,80))
    if bBkO:
        img.draw_rectangle(bBkO)
        print("Brass Black")
        GoodPart()
    #elif dbBkO:
    #    img.draw_rectangle(dbBkO)
    #    print("Brass Black")
    #    GoodPart()

    ssBkO = img.find_template(ssBlackOring, 0.60, step=2, search=SEARCH_EX , roi=(45,20,65,80))
    #dssBkO = img.find_template(dssBlackOring, 0.60, step=2, search=SEARCH_EX , roi=(45,20,65,80))
    if ssBkO:
        img.draw_rectangle(ssBkO)
        print("SS Black Oring")
        GoodPart()
    #elif dssBkO:
    #    img.draw_rectangle(dssBkO)
    #    print("SS Black Oring")
    #   GoodPart()

    ssBnO = img.find_template(ssBrownOring, 0.60, step=2, search=SEARCH_EX , roi=(45,20,65,80))
    #dssBnO = img.find_template(dssBrownOring, 0.60, step=2, search=SEARCH_EX , roi=(45,20,65,80))
    #ddssBnO = img.find_template(darkdarkssBrownOring, 0.65, step=2, search=SEARCH_EX , roi=(45,20,65,80))
   # ddssBnO2 = img.find_template(darkdarkssBrownOring2, 0.60, step=2, search=SEARCH_EX , roi=(45,20,65,80))
    if ssBnO:
        img.draw_rectangle(ssBnO)
        print("SS Brown Oring")
        GoodPart()
    #elif dssBnO:
    #    img.draw_rectangle(dssBnO)
    #    print("SS Brown Oring")
    #    GoodPart()
    #elif ddssBnO:
    ##    img.draw_rectangle(ddssBnO)
     #   print("SS Brown Oring")
     #   GoodPart()
    #elif ddssBnO2:
     #   img.draw_rectangle(ddssBnO2)
      #  print("SS Brown Oring")
       # GoodPart()

    ssRO = img.find_template(ssRedOring, 0.60, step=2, search=SEARCH_EX , roi=(45,20,65,80))
    #dssRO = img.find_template(dssRedOring, 0.60, step=2, search=SEARCH_EX , roi=(45,20,65,80))
    if ssRO:
        img.draw_rectangle(ssRO)
        print("SS Red Oring")
        GoodPart()
    #elif dssRO:
    #    img.draw_rectangle(dssRO)
    #    print("SS Red Oring")
    #    GoodPart()

    ssPo = img.find_template(ssPurpleOring, 0.60, step=2, search=SEARCH_EX , roi=(45,20,65,80))
    #dssPo = img.find_template(dssPurpleOring, 0.60, step=2, search=SEARCH_EX , roi=(45,20,65,80))
    if ssPo:
        img.draw_rectangle(ssPo)
        print("SS Purple Oring")
        GoodPart()
    #elif dssPo:
     #   img.draw_rectangle(dssPo)
     #   print("SS Purple Oring")
     #   GoodPart()






#    print(clock.fps())
